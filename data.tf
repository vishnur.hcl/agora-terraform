#Some data source creation
data "aws_vpc" "eks" {
  id = module.vpc.vpc_id
}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.eks.id]
  }

  tags = {
    Name = "${var.cluster-name}-eks-private"
  }
}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.eks.id]
  }

  tags = {
    Name = "${var.cluster-name}-eks-public"
  }
}

data "aws_security_group" "cluster" {
  vpc_id = data.aws_vpc.eks.id
  name   = module.cluster-sg.security_group_name
}

data "aws_security_group" "node" {
  vpc_id = data.aws_vpc.eks.id
  name   = module.node-sg.security_group_name
}

data "aws_ami" "eks-worker-ami" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${var.k8s-version}-*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon
}